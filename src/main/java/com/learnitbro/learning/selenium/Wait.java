package com.learnitbro.learning.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Wait {

	public void lesson() {

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		// Element & Locator
		By locator = By.xpath("//a[text()='Count Up']");
		WebElement element = driver.findElement(locator);

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		// Page load time

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Waits for xpaths to load on the DOM of the page

		WebDriverWait waiting = new WebDriverWait(driver, 10);
		waiting.until(ExpectedConditions.visibilityOf(element));
		waiting.until(ExpectedConditions.visibilityOfElementLocated(locator));
		waiting.until(ExpectedConditions.invisibilityOf(element));
		waiting.until(ExpectedConditions.presenceOfElementLocated(locator));
		waiting.until(ExpectedConditions.alertIsPresent());
		waiting.until(ExpectedConditions.elementToBeClickable(locator));

	}

}
