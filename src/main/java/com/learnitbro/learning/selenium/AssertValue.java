package com.learnitbro.learning.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AssertValue {

	public void lesson() {

		// Asserts

		String abc = "Mohamed";

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl); // Gets the current url of the page

		// HARD ASSERT
		Assert.assertTrue(abc == "cascsc");
		Assert.assertTrue(currentUrl.equals("https://www.savcaskjvbask.com/"));
		Assert.assertTrue(!currentUrl.equals("https://www.savcaskjvbask.com/"));
		Assert.assertFalse(false);
		Assert.assertEquals("https://www.google.com/", currentUrl);
		
		// SOFT ASSERT
		
		SoftAssert softAssertion= new SoftAssert();
		softAssertion.assertTrue(driver.getCurrentUrl().contains("google"));
		System.out.print("Ignore failure above");
		softAssertion.assertAll();

	}

}
