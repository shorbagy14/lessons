package com.learnitbro.learning.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
	
	private String browser = "chrome"; 
	
	public void lesson() {
		WebDriver driver = getDriver();
		driver.get("https://www.learnitbro.com");
	}
	
	public WebDriver getDriver() {
		WebDriver driver = null;
		switch (browser.toLowerCase()) {
		case "chrome":
			// WebDriverManager.chromedriver().setup();
			// Detect browser version on computer auto
			WebDriverManager.chromedriver().version("73").setup();
			driver = new ChromeDriver();
			break;
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		default:
			System.out.println("Wrong Browser");
			break;
		}
		return driver;
	}

}
