package com.learnitbro.learning.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Element {

	public void lesson() {

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		By locator = By.xpath("//a[text()='Contact']/..");
		WebElement element = getDisplayedElement(locator, driver);
		element.click();
	}
	
	public static WebElement getDisplayedElement(By locator, WebDriver driver) {
		List<WebElement> items = driver.findElements(locator);
		System.out.println(items);
		for (int x = 0; x < items.size(); x++) {
			if (items.get(x).isDisplayed()) {
				return items.get(x);
			}
		}
		return null;
	}
}
