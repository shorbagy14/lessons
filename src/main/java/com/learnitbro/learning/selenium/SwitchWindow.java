package com.learnitbro.learning.selenium;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SwitchWindow {

	public void lesson() {

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.learnitbro.com");
		
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl); // Gets the current url of the page
		
		ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab.get(1));
		Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Learn It Bro"));
		driver.close();
		driver.switchTo().window(tab.get(0));

	}

}
