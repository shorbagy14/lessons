package com.learnitbro.learning.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Activity {
	
	public void lesson() {
		// Normal Activities
		
		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();
		
		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver(); 
		
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl); // Gets the current url of the page

		String title = driver.getTitle(); // Gets the title of the page
		System.out.println(title);
		
		WebElement element = driver.findElement(By.xpath("//a[text()='Gmail']"));
		element.click(); // Clicks on an element
		element.clear(); // Clears a text box
		element.isDisplayed(); // Checks if an element is visible
		element.isEnabled(); // Checks if an element is clickable
		element.isSelected(); // Checks if an element (radio/check box) is selected
		element.sendKeys("Mohamed"); // Sends text
		element.submit(); // Enter keyboard simulation
		
		WebElement upload = driver.findElement(By.xpath("//input[@type='file']"));
		upload.sendKeys("/Users/shorbagy14/Downloads/lesson.txt"); // Upload files

	}

}
