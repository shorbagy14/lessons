package com.learnitbro.learning.selenium;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LinksCheck {
	
	public void lesson() throws IOException {

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.get("https://www.amazon.com");

		int nullLinks = 0;
		int passLinks = 0;
		int failLinks = 0;
		int NotLinks = 0;

		List<WebElement> links = driver.findElements(By.tagName("a"));
		for (WebElement link : links) { // Start of for loop
			String href = link.getAttribute("href");
			System.out.println(href);
			if (href == null) {
				nullLinks++; // Adds how many null (dead) links with each iteration
			} else { // Start of else stat

				URL url = null;
				try { // Start of try and catch
					url = new URL(href); // Convert String to Url

					HttpURLConnection connect = (HttpURLConnection) url.openConnection(); // Creating the connection and
																							// open the ports
					connect.setConnectTimeout(7000); // Set time out
					connect.connect(); // Start the connection and pinging

					if (connect.getResponseCode() <= 399) {
						System.out.println(href + " is Pass");
						passLinks++; // Adds how many links pass with each iteration
					} else {
						System.out.println(href + " is " + connect.getResponseCode());
						failLinks++; // Adds how many links fail with each iteration
					}
				} catch (Exception e) {
					NotLinks++;
				} // Ends of try and catch
			} // End of else stat
		} // Ends of for loop

		// Prints all the results
		System.out.println("Number of null links are: " + nullLinks);
		System.out.println("Number of pass links are: " + passLinks);
		System.out.println("Number of fail links are: " + failLinks);
		System.out.println("Number of not links are: " + NotLinks);

	}
}