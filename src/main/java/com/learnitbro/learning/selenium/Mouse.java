package com.learnitbro.learning.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Mouse {

	public void lesson() {
		// Actions
		//hello again
		
		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();
				
		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver(); 
		
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//a[text()='Count Up']"));
		
		// Clicks and hold the mouse, then move the cursor 10 px (right and down) and then release 
		action.clickAndHold(element).moveByOffset(10, 10).release().build().perform();		
		
		// right click
		action.contextClick(element).build().perform(); 
		
		// double click
		action.doubleClick().build().perform();
		
		// hover
		action.moveToElement(element).build().perform(); 
				
	}

}
