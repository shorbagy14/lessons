package com.learnitbro.learning.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Scroll {
	
	public void lesson() {

		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.learnitbro.com");
		
		WebElement element = driver.findElement(By.xpath("//h2[text()='Our Services']"));
		String command = "arguments[0].scrollIntoView(true);";
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(command, element);
		
	}
}
