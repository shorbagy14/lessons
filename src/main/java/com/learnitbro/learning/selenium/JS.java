package com.learnitbro.learning.selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class JS {
	
	public void lesson() {
		
		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.learnitbro.com");
		
		driver.manage().window().maximize();
		driver.manage().window().fullscreen();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		
		// Document ready
		String readyState = js.executeScript("return document.readyState").toString(); // return method
		System.out.println(readyState);
		
		js.executeScript("return document.readyState"); // void method
		
	}
}
