package com.learnitbro.learning.testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Setup {
	
	public WebDriver driver;
	
	@BeforeTest
	@Parameters(value= {"browser", "version"})
	public void beforetestApp(String browser, String version) {
		switch (browser.toLowerCase()) {
		case "chrome":
			// WebDriverManager.chromedriver().setup();
			// Detect browser version on computer auto
			WebDriverManager.chromedriver().version(version).setup();
			driver = new ChromeDriver();
			break;
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		default:
			System.out.println("Wrong Browser");
			break;
		}
	}

	@Test
	public void testApp() {
		driver.get("https://www.google.com");
		Assert.assertTrue(driver.getCurrentUrl().contains("google"));
	}
	
	@AfterTest
	public void afterTestApp() {
		driver.quit();
	}

}
