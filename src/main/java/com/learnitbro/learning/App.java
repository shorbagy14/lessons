package com.learnitbro.learning;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class App {
	public static void main(String[] args) throws IOException {
		App app = new App();
		app.lesson();
	}

	public void lesson() throws IOException {
		
		// Downloads and trigger the Chrome driver version 73
		WebDriverManager.chromedriver().version("73").setup();

		// Defining the driver to allow you to use for further activities
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.get("https://www.amazon.com");
		
		Assert.assertTrue(driver.getCurrentUrl().contains("google"));		
		System.out.println("I made it here");

	}
}
