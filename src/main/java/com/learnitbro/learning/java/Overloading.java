package com.learnitbro.learning.java;

public class Overloading {
	
	public void lesson() {
		int i = add(1,1);
		double d = add(2.0, 2.0);
		String n = add("Mohamed ", "Elshorbagy");
		
		System.out.println(i);
		System.out.println(d);
		System.out.println(n);
	}

	// Method overloading
	private static int add(int a, int b) {
		return a + b;
	}

	private static double add(double a, double b) {
		return a + b;
	}

	private static String add(String a, String b) {
		return a + " " + b;
	}
}
