package com.learnitbro.learning.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArrayExample {

	public void lesson() {
		String[] members = { "Alex", "Sarah", "John" };

		if (Arrays.asList(members).contains("Mohamed")) {
			System.out.println("Match");
		} else {
			System.out.println("This list doesnt have the value");
		}
	}

	public void other() {
		// Collections

		List<String> list = new ArrayList<String>(); // Duplicates allowed
		list.add("123");
		list.add("4324");
		list.add("Mohamed");
		
		list.remove("123");

		for (String num : list) {
			System.out.println(num);
		}

		// ArrayList // Doesnt care about order
		// LinkedList // Orders them auto
		// Vector // xxx
		// Stack // xxx

		Set<Double> set = new HashSet<Double>(); // No Duplicates
		set.add(1.245);
		
		// HashSet // Doesnt care about order
		// LinkedHashSet // Orders them auto
		// TreeSet // Order based on your input
	}

}
