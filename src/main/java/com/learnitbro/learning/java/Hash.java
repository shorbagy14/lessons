package com.learnitbro.learning.java;

import java.util.HashMap;
import java.util.HashSet;

public class Hash {

	public void lesson() {

		HashSet<String> abc = new HashSet<String>();
		abc.add("Mohamed");
		abc.add("Blen");
		abc.add("Mohamed");

		System.out.println(abc);

		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(5472, "Mohamed Elshorbagy");
		hm.put(5479, "Blen John");
		// hm.put(5472, "Andrew Smith");

		System.out.println(hm);
		System.out.println(hm.get(5472));
	}
}
