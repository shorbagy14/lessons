package com.learnitbro.learning.java;

public class Casting {
	
	public void lesson() {
		int abc = 199;
		double num = (double) abc;
		System.out.print(num);
	}

}
