package com.learnitbro.learning.java;

public class Catch {

	String[] names = { "Mohamed", "Ahmed", "John" };

	public void lesson() {

		try {

			for (int x = 0; x < 10; x++) {
				System.out.println(names[x]);
			}
			
			System.out.println("Code ran well");
			
		} catch (Exception e) {
			System.out.println("Error happened");
			e.printStackTrace();
		} finally {
			System.out.println("I am gonna run either way coz I am cool");
		}
	}
}
