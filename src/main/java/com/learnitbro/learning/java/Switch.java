package com.learnitbro.learning.java;

public class Switch {

	public void lesson() {
		String name = "Mohamed";
		String dayofWeek;

		switch (name) {
		case "Mohamed":
			dayofWeek = "Monday";
			break;
		case "Blen":
			dayofWeek = "Tuesday";
			break;
		case "John":
			dayofWeek = "Wednesday";
			break;
		default:
			dayofWeek = "This day doesnt exist";
			break;
		}
		System.out.println(dayofWeek);
	}

}
