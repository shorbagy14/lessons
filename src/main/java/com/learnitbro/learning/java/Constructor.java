package com.learnitbro.learning.java;

public class Constructor {

	private String name;

	/*
	 * A constructor is a special method that is used to initialize a newly created
	 * object and is called just after the memory is allocated for the object. It
	 * can be used to initialize the objects to desired values or default values at
	 * the time of object creation
	 * 
	 * 
	 * @link: https://www.guru99.com/java-constructors.html
	 */
	
	public void lesson() {
		Constructor person = new Constructor("Mohamed");
		person.sleeping();
		person.napping();
		person.working();
	}

	public Constructor(String nameOfTheMember) {
		name = nameOfTheMember;
	}

	public void working() {
		System.out.println(name + " is working");
	}

	public void sleeping() {
		System.out.println(name + " is sleeping");
	}

	public void napping() {
		System.out.println(name + " is napping");
	}

}
