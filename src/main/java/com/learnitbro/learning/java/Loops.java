package com.learnitbro.learning.java;

public class Loops {
	
	String[] names = {"Mohamed", "Ahmed", "Sarah"};
	int number = 1;

	public void lesson() {
		// Regular for loop
		for (int x = 0; x < names.length; x++) {
			System.out.println(names[x]);
		}

		// Enhanced for loop
		for (String item : names) {
			System.out.println(item);
		}
		
		// Do loop
		do {
			System.out.println("true");
		} while(false);
		
		// While loop
		while(true) {
			System.out.println("true");
			if(number == 1) {
				break;
			}
		}
	}

}
