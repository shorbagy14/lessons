package com.learnitbro.learning;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class AppTest {
	
	public WebDriver driver;
	
	/* 
	 * @description: 
	 * @name: beforetestApp
	 * @param: browser (String)
	 * @param: version (String)
	 * 
	 * @return = value of something
	 * @author: Mohamed
	 */
	
	@BeforeTest
	@Parameters(value= {"browser", "version"})
	public void beforetestApp(String browser, String version) {
		switch (browser.toLowerCase()) {
		case "chrome":
			// WebDriverManager.chromedriver().setup();
			// Detect browser version on computer auto
			WebDriverManager.chromedriver().version(version).setup();
			driver = new ChromeDriver();
			break;
		case "firefox":
//			WebDriverManager.firefoxdriver().setup();
			WebDriverManager.firefoxdriver().version(version).setup();
			driver = new FirefoxDriver();
			break;
		default:
			System.out.println("Wrong Browser");
			break;
		}
	}

//	@Test (groups= {"group-1", "group-2"})
	
//	@Test (priority = 0)
//	public boolean testApp1() {
//		driver.get("https://www.google.com");
//		Assert.assertTrue(driver.getCurrentUrl().contains("google"));
//		return true;
//	}
	
	// CMD + ? = Comments the section
	
	@Test (priority = 0, description= "This method runs google test")
	public void testApp1() {
		driver.get("https://www.google.com");
		Assert.assertTrue(driver.getCurrentUrl().contains("asvsdvasdv"), "This checks for the google url");
//		Assert.assertTrue(true, "String");
	}
	
	
//	public void app(){
//		if(testApp1()) {
//			testApp2();
//		}
//	}
	
//	@Test (priority = 1)
//	@Test (dependsOnGroups = {"group-1"})
//	@Test (dependsOnMethods = {"testApp1"}, priority = 1)
//	public void testApp2() {
//		driver.get("https://www.cnn.com");
//		Assert.assertTrue(driver.getCurrentUrl().contains("cnn"));
//	}
	
	@AfterTest
	public void afterTestApp() {
		driver.quit();
	}
}
